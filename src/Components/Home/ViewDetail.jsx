import React from 'react'

function ViewDetail(data) {

    var dataView = data.data.find((d)=> d.id === data.match.params.id)
    console.log(dataView);
    

    return (
        <div className="container" style={{marginTop:30}}>
            <div className="row" style={{padding:30,backgroundColor:"#4f8a8b",borderRadius:20}}>
                <div className="col-md-4">
                    <img className="w-100" src={dataView.img} alt="Movie"/>
                </div>
                <div className="col-md-8" style={{color:"#f4f4f4"}}>
                    <h1>{dataView.title}</h1>
                    <h5>ID: {dataView.id}</h5>
                    <hr style={{backgroundColor:"#f4f4f4"}}></hr>
                    <p>{dataView.description}</p>
                </div>
            </div>
        </div>
    )
}

export default ViewDetail
