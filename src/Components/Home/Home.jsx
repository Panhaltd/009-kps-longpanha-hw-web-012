import { Link } from "react-router-dom";
import React from "react";
import {Card,Button} from 'react-bootstrap'

function Home(props) {
    let items = props.data.map((item)=>
        <div key={item.id} className="col-md-3"  style={{marginBottom:20}}>
            <Card>
            <Card.Img variant="top" src={item.img} />
            <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>
                {item.description}
                </Card.Text>
                <Link to={`/ViewDetail/${item.id}`}><Button style={{backgroundColor:"#0a97b0"}}>View Detail</Button></Link>
            </Card.Body>
            </Card>
        </div>
    )
    return (
        <div className="container" style={{padding:30}}>
            <div className="row">
                {items}
            </div>
        </div>
    )
}

export default Home
