import React, { Component } from 'react'
import { Link, Switch, Route } from 'react-router-dom'
import AccountType from './AccountType'

export default class Account extends Component {
    render() {
        return (
            <div style={{padding:20}}>
                <ul style={{float:"left"}}>
                <li>
                    <Link to="/Account/Netflix">Netflix</Link>
                </li>
                <li>
                    <Link to="/Account/Zillow">Zillow Group</Link>
                </li>
                <li>  
                    <Link to="/Account/Yahoo">Yahoo</Link>
                </li>
                <li>
                    <Link to="/Account/Modus">Modus Create</Link>
                </li>
            </ul>
            <div style={{float:"left",width:"100%",textAlign:"left"}}>
            <Switch>
                <Route exact path="/Account"><h2>Please Choose A Topic:</h2></Route>
                <Route path="/Account/:topic" component={AccountType}/>
            </Switch>
            </div>
            </div>
        )
    }
}
