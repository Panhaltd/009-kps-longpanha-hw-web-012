import React from 'react'
import { useParams } from 'react-router-dom';

function AccountType() {

    let topicID = useParams().topic;

    return (
        <div>
            <h2>The <span style={{color:"#f54291"}}>name</span> in the query string is "{topicID}"</h2>
        </div>
    )
}

export default AccountType
