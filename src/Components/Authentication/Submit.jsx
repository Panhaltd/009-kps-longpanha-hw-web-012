class Auth {
    constructor() {
      this.submitted = false;
    }
  
    login(key) {
      this.submitted = true;
      key();
    }
  
    logout(key) {
      this.submitted = false;
      key();
    }
  
    isSubmitted() {
      return this.submitted;
    }
  }
  
  export default new Auth();
  