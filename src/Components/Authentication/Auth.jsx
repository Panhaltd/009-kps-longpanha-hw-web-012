import { useHistory } from 'react-router-dom'

import Authen from './Submit'
import React from 'react'
export default function Auth(props) {
    let histroy=useHistory();
    let username="";
    let password="";
    function getUsername(event){
       username=event.target.value;
    }
    function getPassword(event){
        password=event.target.value
    }
    function redirect(){
        if(username!=="" && password!==""){
            Authen.login(()=>{
                histroy.push("/welcome");
            })
        } 
    }
    return (
      <div style={{padding:20}}>
        <input style={{padding:5}} type="text" placeholder="Username" onChange={(e)=>getUsername(e)} />
        <input style={{padding:5}} type="Password" placeholder="Password" onChange={(e)=>getPassword(e)}/>
        <input style={{padding:5}} type="submit" variant="primary" onClick={()=>redirect()} value="Submit"/>
      </div>              
    )
}


