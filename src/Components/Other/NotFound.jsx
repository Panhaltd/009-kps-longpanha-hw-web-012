import React from 'react'

function NotFound() {
    return (
        <div className="App">
            <h1>! Error Not Found 404 !</h1>
        </div>
    )
}

export default NotFound
