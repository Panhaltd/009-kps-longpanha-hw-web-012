import React from 'react'
import { useParams } from 'react-router-dom';

function AnimationType() {
    let topicID = useParams().topic;
    return (
        <div>
            <h2>{topicID}</h2>
        </div>
    )
}

export default AnimationType
