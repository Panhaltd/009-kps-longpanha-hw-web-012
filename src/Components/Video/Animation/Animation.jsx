import React from 'react'
import { Link, Switch, Route } from 'react-router-dom'
import AnimationType from './AnimationType'

function Animation() {
    return (
        <div>
            <ul style={{float:"left"}}>
                <li>
                    <Link to="/Video/Animation/Action">Action</Link>
                </li>
                <li>
                    <Link to="/Video/Animation/Romance">Romance</Link>
                </li>
                <li>
                    <Link to="/Video/Animation/Comedy">Comedy</Link>
                </li>
            </ul>
            <div style={{float:"left",width:"100%",textAlign:"left"}}>
            <Switch>
                <Route exact path="/Video/Animation"><h2>Please Choose A Topic:</h2></Route>
                <Route path="/Video/Animation/:topic" component={AnimationType}/>
            </Switch>
            </div>
        </div>
    )
}


export default Animation
