import React from 'react'
import { useParams, Switch, Route } from 'react-router-dom'
import Animation from './Animation/Animation';
import Movie from './Movie/Movie';

function VideoType() {

    let topicID = useParams().topic;
    return (
        <div>
            <h2>{topicID}</h2>
            <Switch>
                <Route path="/Video/Animation" component={Animation}/>
                <Route path="/Video/Movie" component={Movie}/>
            </Switch>
        </div>
    )
}

export default VideoType
