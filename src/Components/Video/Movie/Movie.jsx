import React from 'react'
import { Link, Switch, Route } from 'react-router-dom'
import MovieType from './MovieType'

function Movie() {
    return (
        <div>
            <ul style={{float:"left"}}>
                <li>
                    <Link to="/Video/Movie/Adventure">Adventure</Link>
                </li>
                <li>
                    <Link to="/Video/Movie/Comedy">Comedy</Link>
                </li>
                <li>
                    <Link to="/Video/Movie/Crime">Crime</Link>
                </li>
                <li>
                    <Link to="/Video/Movie/Documentary">Documentary</Link>
                </li>
            </ul>
            <div style={{float:"left",width:"100%",textAlign:"left"}}>
            <Switch>
                <Route exact path="/Video/Movie"><h2>Please Choose A Topic:</h2></Route>
                <Route path="/Video/Movie/:topic" component={MovieType}/>
            </Switch>
            </div>
        </div>
    )
}

export default Movie
