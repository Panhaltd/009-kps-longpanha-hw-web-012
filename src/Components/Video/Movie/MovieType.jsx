import React from 'react'
import { useParams } from 'react-router-dom';

function MovieType() {
    let topicID = useParams().topic;
    return (
        <div>
            <h2>{topicID}</h2>
        </div>
    )
}

export default MovieType
