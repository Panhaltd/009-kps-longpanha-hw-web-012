import React from 'react'
import { Link, Switch, Route } from 'react-router-dom'
import VideoType from './VideoType'

function Video() {
    return (
        <div style={{padding:20}}>
            <ul style={{float:"left"}}>
                <li>
                    <Link to="/Video/Animation">Animation</Link>
                </li>
                <li>
                    <Link to="/Video/Movie">Movie</Link>
                </li>
            </ul>
            <div style={{float:"left",width:"100%",textAlign:"left"}}>
            <Switch>
                <Route exact path="/Video"><h2>Please Choose A Topic:</h2></Route>
                <Route path="/Video/:topic" component={VideoType}/>
            </Switch>
            </div>
        </div>
    )
}
export default Video
