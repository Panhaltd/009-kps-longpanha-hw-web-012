import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Menu from './Components/Other/Menu';
import Home from './Components/Home/Home'
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Account from './Components/Account/Account';
import Auth from './Components/Authentication/Auth';
import Video from './Components/Video/Video';
import NotFound from './Components/Other/NotFound';
import React, { Component } from 'react'
import ViewDetail from './Components/Home/ViewDetail';
import MainPage from './Components/Other/MainPage';
import { ProtectedRoute } from './Components/Authentication/Proected';
import Welcome from './Components/Authentication/Welcome';

export default class App extends Component {

  constructor(){
    super();
    this.state={
      data: [
        {
          id: 1,
          title: "Avengers Endgame",
          description:
            "Anna sets out on a journey with an iceman, Kristoff, and his reindeer, Sven, in order to find her sister",
          img:
            "https://upload.wikimedia.org/wikipedia/en/0/0d/Avengers_Endgame_poster.jpg",
        },
        {
          id: 2,
          title: "Joker",
          description:
            "When Snow White, a princess, is exiled by her stepmother, an evil queen who wants to kill her, she runs",
          img:
            "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
        },
        {
          id: 3,
          title: "Extraction",
          description:
            "A black-market mercenary who has nothing to lose is hired to rescue the kidnapped son of an imprisoned ",
          img:
            "https://upload.wikimedia.org/wikipedia/en/8/89/Extraction_%282020_film%29.png",
        },
        {
          id: 4,
          title: "Godzilla",
          description:
            "The new story follows the heroic efforts of the crypto-zoological agency Monarch as its members face off",
          img: "https://www.1loy.com/folders/images/2019/8/16602/16602.png",
        },
        {
          id: 5,
          title: "Avengers Endgame",
          description:
            "Anna sets out on a journey with an iceman, Kristoff, and his reindeer, Sven, in order to find her sister",
          img:
            "https://upload.wikimedia.org/wikipedia/en/0/0d/Avengers_Endgame_poster.jpg",
        },
        {
          id: 6,
          title: "Joker",
          description:
            "When Snow White, a princess, is exiled by her stepmother, an evil queen who wants to kill her, she runs",
          img:
            "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
        },
        {
          id: 7,
          title: "Extraction",
          description:
            "A black-market mercenary who has nothing to lose is hired to rescue the kidnapped son of an imprisoned ",
          img:
            "https://upload.wikimedia.org/wikipedia/en/8/89/Extraction_%282020_film%29.png",
        },
        {
          id: 8,
          title: "Godzilla",
          description:
            "The new story follows the heroic efforts of the crypto-zoological agency Monarch as its members face off",
          img: "https://www.1loy.com/folders/images/2019/8/16602/16602.png",
        },
      ]
    }
  }

  render() {
    return (
      <div className="App">
        <Router>
          <Menu/>
          <Switch>
            <Route exact path="/" component={MainPage}/>
            <Route path="/Home" component={()=><Home data={this.state.data}/>}/>
            <Route path="/Video" component={Video}/>
            <Route path="/Account" component={Account}/>
            <Route path="/Auth" component={Auth}/>
            <ProtectedRoute exact path="/welcome" component={Welcome}/>
            <Route path="/ViewDetail/:id" render={(props)=><ViewDetail {...props} data={this.state.data}/>}/>
            <Route path="*" component={NotFound}/>
          </Switch>
        </Router>
      </div>
    )
  }
}
